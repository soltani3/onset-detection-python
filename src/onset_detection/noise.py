import numpy as np


class NoisyDataGenerator:
    """Class to generate noisy signal.

    Class to generate noisy IMU signal

    Attributes:
        noise_ratio = the ratio between zero and one to add noise to the signal

    """

    def __init__(self, noise_ratio=0.05):
        self.noise_ratio = noise_ratio

    def apply(self, data):
        """Generate noisy IMU data

        Args:
            data: a dataframe containing sensory data

        """
        sensor_columns = [column for column in data.columns
                          if 'time' not in column]
        seeds = np.random.permutation(len(sensor_columns))
        for sensor_column, seed in zip(sensor_columns, seeds):
            signal_mean = np.nanmean(data[sensor_column])
            signal_std = np.nanstd(data[sensor_column])
            np.random.seed(seed)
            noise = np.random.normal(
                loc=signal_mean, scale=signal_std, size=data.shape[0])
            data[sensor_column] = ((1 - self.noise_ratio) * data[sensor_column]
                                   + self.noise_ratio * noise)
        return data
