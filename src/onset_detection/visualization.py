from plotly.subplots import make_subplots
from .util import add_norm
import os
import numpy as np


class Plotter:
    """Class to visualize data and the results of Onset detection"""

    _FEATURES_GROUP = {
        'time': ['acc_mean', 'acc_std', 'acc_min', 'acc_max', 'acc_p2p'],
        'frequency': ['acc_df', 'acc_df_amp', 'acc_auc_peak',
                      'acc_auc_interval', 'acc_zc'],
        'gyr': ['gyr_mean', 'gyr_std', 'gyr_df', 'gyr_df_amp']
    }

    def __init__(self, config, name="", exclude_signals=None, font_size=18):
        if exclude_signals is None:
            exclude_signals = ['time']
        self.config = config
        self.name = name
        self.exclude_signals = exclude_signals
        self.font_size = font_size

    def plot_data(self, data, results=None):
        """Visualize signals"""
        data = add_norm(data)
        data_columns = [column for column in data.columns
                        if column not in self.exclude_signals + ['time']]
        acc_columns = [column for column in data_columns if 'acc_' in column]
        gyr_columns = [column for column in data_columns if 'gyr_' in column]
        mag_columns = [column for column in data_columns if 'mag_' in column]
        rows = int(np.sum([len(acc_columns) > 0, len(gyr_columns) > 0,
                           len(mag_columns) > 0]))
        data_time_ax = data['time'] - data['time'].iloc[0]
        specs = [[{"secondary_y": True}]]
        for i in range(rows - 1):
            specs.append([{"secondary_y": False}])
        fig = make_subplots(rows=rows, cols=1, shared_xaxes=True, specs=specs)
        row = 0
        for columns, sensor in zip([acc_columns, gyr_columns, mag_columns],
                                   ['acc', 'gyr', 'mag']):
            row = row + 1
            info = self._get_sensor_info(sensor)
            for signal in columns:
                fig.add_scatter(x=data_time_ax, y=data[signal], name=signal,
                                row=row, col=1, secondary_y=False)
            fig.update_yaxes(title_text=info['label'], range=info['range'],
                             row=row, col=1, secondary_y=False)
        if results is not None:
            result_time_ax = results['time'] - data['time'].iloc[0]
            fig.add_scatter(x=result_time_ax,
                            y=results['detection'], name="detection",
                            line_width=2, line_color="black", row=1, col=1,
                            secondary_y=True)
            fig.update_yaxes(categoryorder='array',
                             categoryarray=[False, True],
                             tickmode='array', tickvals=[False, True],
                             row=1, col=1,
                             secondary_y=True)
        fig.update_xaxes(title_text='Time, s', row=rows, col=1)
        fig.update_layout(title_text=f"Data visualization of {self.name}",
                          font_size=self.font_size)
        return fig

    @staticmethod
    def _get_sensor_info(sensor):
        if 'acc' in sensor:
            info = {'label': 'g', 'range': [-3, 3]}
        elif 'gyr' in sensor:
            info = {'label': 'd/s', 'range': [-250, 250]}
        elif 'mag' in sensor:
            info = {'label': 'uT', 'range': [-15000, 15000]}
        return info

    def plot_features_series(self, data, features, save_directory,
                             feature_list='all'):
        """Visualize the features as time series"""
        if 'all' in feature_list:
            feature_list = [column for column in features.columns]
        acc_columns = [column for column in data.columns if 'acc' in column]
        data_time = data['time'] - data['time'].iloc[0]
        features_time = features['time'] - data['time'].iloc[0]
        for key in self._FEATURES_GROUP.keys():
            features_names = [feature for feature
                              in self._FEATURES_GROUP[key]
                              if feature in feature_list]
            if len(features_names) > 0:
                rows = len(features_names) + 1
                fig = make_subplots(rows=rows, cols=1, shared_xaxes=True)
                for acc_column in acc_columns:
                    fig.add_scatter(x=data_time, y=data[acc_column],
                                    row=1, col=1, name=acc_column)
                fig.update_yaxes(title_text="g", row=1, col=1)
                for i, features_name in enumerate(features_names):
                    fig.add_scatter(x=features_time, y=features[features_name],
                                    row=i + 2, col=1, name=features_name)
                fig.update_xaxes(title_text="Time, s",
                                 row=len(features_names) + 1, col=1)
                fig.update_layout(
                    title_text=f"{key} features of {self.name}",
                    font_size=self.font_size)
                fig.write_html(
                    os.path.join(save_directory, f"features_{key}.html"))
