import click
import os
import json
import pandas as pd

from .datasets.TESTDATA.data import TestDataRecord
from .util import read_config
from .visualization import Plotter
from .algorithm import OnsetDetector
from .features import FeatureExtractor
from .noise import NoisyDataGenerator


_EXCLUDE_SUBJECTS = []
_EXCLUDE_SIGNALS = []
_DEFAULT_PURPOSE = ""
_DEFAULT_RUN = "run_5"
_IF_LOCAL_MACHINE = True

if _IF_LOCAL_MACHINE:
    _DEFAULT_RESULT_PATH = r"D:\Projects\example\results\test_data"
    _DEFAULT_DATA_PATH = r"D:\Projects\example\data\test_data\RawData"
    _DEFAULT_PROCESSED_DATA_PATH = r"D:\Projects\example\data" \
                                   r"\test_data\ProcessedData"
    _DEFAULT_PROCESSED_NOISY_DATA_PATH = r"D:\Projects\example\data"\
                                         r"\test_data\NoisyProcessedData"
    _DEFAULT_RUN_PATH = os.path.join(_DEFAULT_RESULT_PATH, _DEFAULT_RUN)
else:
    _DEFAULT_RESULT_PATH = r""
    _DEFAULT_DATA_PATH = r""
    _DEFAULT_PROCESSED_DATA_PATH = r""
    _DEFAULT_PROCESSED_NOISY_DATA_PATH = r""
    _DEFAULT_RUN_PATH = os.path.join(_DEFAULT_RESULT_PATH, _DEFAULT_RUN)
if not os.path.isdir(_DEFAULT_RUN_PATH):
    os.makedirs(_DEFAULT_RUN_PATH)
_DEFAULT_CONFIG_PATH = os.path.join(_DEFAULT_RUN_PATH, 'config.json')
if not os.path.isdir(_DEFAULT_PROCESSED_DATA_PATH):
    os.makedirs(_DEFAULT_PROCESSED_DATA_PATH)
_DEFAULT_FEATURES_PATH = os.path.join(_DEFAULT_RUN_PATH, 'features')
if not os.path.isdir(_DEFAULT_FEATURES_PATH):
    os.makedirs(_DEFAULT_FEATURES_PATH)
_DEFAULT_DETECTION_PATH = os.path.join(_DEFAULT_RUN_PATH, 'output')
if not os.path.isdir(_DEFAULT_DETECTION_PATH):
    os.makedirs(_DEFAULT_DETECTION_PATH)
if not os.path.isdir(_DEFAULT_PROCESSED_NOISY_DATA_PATH):
    os.makedirs(_DEFAULT_PROCESSED_NOISY_DATA_PATH)


@click.group()
def cli():
    """Tools for applying and evaluating onset_detection on IMU signals"""
    pass


@cli.command(name='configure')
@click.option('--fs', type=click.FLOAT, default=25,
              help="Set the sampling frequency in Hz")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_RUN_PATH,
              help="Set the directory to write the configuration file")
@click.option('--run_purpose', type=click.STRING,
              help='Set the purpose of the current run')
@click.option('--features_window_length', type=click.FLOAT, default=4,
              help='Set the length of window used for the feature extraction')
def cli_configure(
        fs, save_directory, run_purpose, features_window_length):
    """Configure a run for the onset detection"""
    config = {
        'purpose': run_purpose,
        'fs': fs,
        'algorithm': {
            'features_window_length': features_window_length,
        }
    }
    config = json.dumps(config, indent=4)
    if not os.path.isdir(save_directory):
        os.makedirs(save_directory)
    with open(os.path.join(save_directory, 'config.json'), "w") as f:
        f.write(config)


@cli.group(name='data')
def cli_data():
    """Tools for preparing data for onset detection"""
    pass


@cli_data.command(name='preprocess')
@click.option('--config_file', type=click.Path(exists=False, file_okay=True),
              default=_DEFAULT_CONFIG_PATH,
              help='Set the path to the configuration file')
@click.option('--subject_id', type=click.STRING, default=None,
              help='Set the name of the file to be processed')
@click.option('--data_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_DATA_PATH,
              help="Set the directory of the raw data")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_PROCESSED_DATA_PATH,
              help="Set the directory to save the processed data")
def cli_data_preprocess(
        config_file, subject_id, data_directory, save_directory):
    """Preprocess the signals"""
    if subject_id is None:
        subjects = os.listdir(data_directory)
        subjects = [directory for directory in subjects
                    if '.json' not in directory
                    and '.csv' not in directory
                    and 's' in directory
                    and directory not in _EXCLUDE_SUBJECTS]
    else:
        subjects = [subject_id]
    with open(config_file, 'r') as f:
        config = json.load(f)
    save_directory = os.path.join(save_directory, f"fs_{config['fs']}")
    if not os.path.isdir(save_directory):
        os.makedirs(save_directory)
    for subject in subjects:
        data = TestDataRecord(
            re_sample=True, config=config,
            exclude_signals=_EXCLUDE_SIGNALS).read(
            os.path.join(data_directory, subject))
        save_path = os.path.join(save_directory, subject)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        data.to_csv(os.path.join(save_path, f'{subject}_data.csv'),
                    index=False)
    with open(os.path.join(save_directory, 'config_data.json'), "w") as f:
        f.write(json.dumps({'fs': config['fs']}))


@cli_data.command(name='noisy')
@click.option('--config_file', type=click.Path(exists=False, file_okay=True),
              default=_DEFAULT_CONFIG_PATH,
              help='Set the path to the configuration file')
@click.option('--subject_id', type=click.STRING, default=None,
              help='Set the name of the file to be processed')
@click.option('--processed_data_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_PROCESSED_DATA_PATH,
              help="Set the directory of the processed data")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_PROCESSED_NOISY_DATA_PATH,
              help="Set the directory to save the noisy data")
def cli_data_noisy(
        config_file, subject_id, processed_data_directory, save_directory):
    """Function to add noise to the signals"""
    config, processed_data_directory = read_config(
        config_file, processed_data_directory)
    if subject_id is None:
        subjects = os.listdir(processed_data_directory)
        subjects = [directory for directory in subjects
                    if '.json' not in directory
                    and directory not in _EXCLUDE_SUBJECTS]
    else:
        subjects = [subject_id]
    save_directory = os.path.join(save_directory, f"fs_{config['fs']}")
    for subject in subjects:
        data = pd.read_csv(os.path.join(processed_data_directory,
                                        subject, f'{subject}_data.csv'))
        data = NoisyDataGenerator(noise_ratio=0.05).apply(data)
        save_path = os.path.join(save_directory, subject)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        data.to_csv(os.path.join(save_path, f'{subject}_data.csv'),
                    index=False)
    with open(os.path.join(save_directory, 'config_data.json'), "w") as f:
        f.write(json.dumps({'fs': config['fs']}))


@cli_data.command(name='plot')
@click.option('--config_file', type=click.Path(exists=False, file_okay=True),
              default=_DEFAULT_CONFIG_PATH,
              help='Set the path to the configuration file')
@click.option('--subject_id', type=click.STRING, default=None,
              help='Set the name of the file to be processed')
@click.option('--processed_data_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_PROCESSED_DATA_PATH,
              help="set the directory to the processed data")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_RUN_PATH,
              help="Set the directory to save the figures")
@click.option('--show', type=click.BOOL,
              help='Set as True to show the figures', default=False)
@click.option('--font_size', type=click.INT, default=18,
              help='Set the font size of the figure')
@click.option('--db_name', type=click.STRING,
              help='Set the name of the database', default="test-data")
def cli_data_plot(
        config_file, subject_id, processed_data_directory, save_directory,
        show, font_size, db_name):
    """Function to plot the raw data"""
    config, processed_data_directory = read_config(
        config_file, processed_data_directory)
    if subject_id is None:
        subjects = os.listdir(processed_data_directory)
        subjects = [directory for directory in subjects
                    if '.json' not in directory
                    and directory not in _EXCLUDE_SUBJECTS]
    else:
        subjects = [subject_id]
    for subject in subjects:
        data = pd.read_csv(os.path.join(processed_data_directory,
                                        subject, f'{subject}_data.csv'))
        fig = Plotter(
            config=config,
            name=f"{subject} at {db_name} database",
            exclude_signals=_EXCLUDE_SIGNALS,
            font_size=font_size).plot_data(data=data, results=None)
        if save_directory is not None:
            save_path = os.path.join(save_directory, 'figures', subject)
            if not os.path.isdir(save_path):
                os.makedirs(save_path)
            fig.write_html(os.path.join(save_path, 'data.html'))
        if show:
            fig.show()


@cli.group(name='features')
def cli_features():
    """Tools for extracting features for onset detection"""
    pass


@cli_features.command(name='extract')
@click.option('--config_file', type=click.Path(exists=False, file_okay=True),
              default=_DEFAULT_CONFIG_PATH,
              help='Set the path to the configuration file')
@click.option('--subject_id', type=click.STRING, default=None,
              help='Set the name of the file to be processed')
@click.option('--processed_data_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_PROCESSED_DATA_PATH,
              help="Set the directory to the processed data")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_RUN_PATH,
              help="Set the directory to save the extracted features")
def cli_features_extract(
        subject_id, processed_data_directory, save_directory, config_file):
    """Function to extract features"""
    config, processed_data_directory = read_config(
        config_file, processed_data_directory)
    if subject_id is None:
        subjects = os.listdir(processed_data_directory)
        subjects = [directory for directory in subjects
                    if '.json' not in directory
                    and directory not in _EXCLUDE_SUBJECTS]
    else:
        subjects = [subject_id]
    for subject in subjects:
        data = pd.read_csv(
            os.path.join(processed_data_directory, subject,
                         f'{subject}_data.csv'))
        features = FeatureExtractor(config=config).apply(data)
        save_path = os.path.join(save_directory, 'features', subject)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        features.to_csv(
            os.path.join(save_path, 'features.csv'), index=False)


@cli_features.command(name='plot')
@click.option('--config_file', type=click.Path(exists=False, file_okay=True),
              default=_DEFAULT_CONFIG_PATH,
              help='Set the path to the configuration file')
@click.option('--subject_id', type=click.STRING, default=None,
              help='Set the name of the file to be processed')
@click.option('--features_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_FEATURES_PATH,
              help="set the directory to the features")
@click.option('--processed_data_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_PROCESSED_DATA_PATH,
              help="Set the directory to the processed data")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_RUN_PATH,
              help="Set the directory to save the figures")
@click.option('--font_size', type=click.INT, default=18,
              help='Set the font size of the figure')
@click.option('--db_name', type=click.STRING,
              help='Name of the database', default="test-data")
def cli_features_plot(
        config_file, subject_id, features_directory, processed_data_directory,
        save_directory, font_size, db_name):
    """Function to visualize the features"""
    config, processed_data_directory = read_config(
        config_file, processed_data_directory)
    if subject_id is None:
        subjects = os.listdir(features_directory)
        subjects = [directory for directory in subjects
                    if '.json' not in directory
                    and directory not in _EXCLUDE_SUBJECTS]
    else:
        subjects = [subject_id]
    for subject in subjects:
        data = pd.read_csv(os.path.join(processed_data_directory,
                                        subject, f'{subject}_data.csv'))
        features = pd.read_csv(
            os.path.join(features_directory, subject, 'features.csv'))
        save_path = os.path.join(save_directory, 'figures', subject)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        Plotter(
            config=config,
            name=f"{subject} at {db_name} database",
            exclude_signals=_EXCLUDE_SIGNALS,
            font_size=font_size).plot_features_series(
            data=data, features=features, save_directory=save_path)


@cli.group(name='algorithm')
def cli_algorithm():
    """Tools for applying onset detection"""
    pass


@cli_algorithm.command(name='apply')
@click.option('--config_file', type=click.Path(exists=False, file_okay=True),
              default=_DEFAULT_CONFIG_PATH,
              help='Set the path to the configuration file')
@click.option('--subject_id', type=click.STRING, default=None,
              help='Set the name of the file to be processed')
@click.option('--features_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_FEATURES_PATH,
              help="Set the directory to the features")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_RUN_PATH,
              help="Set the directory to save the results of onset detection")
def cli_algorithm_apply(
        subject_id, features_directory, save_directory, config_file):
    """Function to apply onset detection on the features"""
    with open(config_file, 'r') as f:
        config = json.load(f)
    if subject_id is None:
        subjects = os.listdir(features_directory)
        subjects = [directory for directory in subjects
                    if '.json' not in directory
                    and directory not in _EXCLUDE_SUBJECTS]
    else:
        subjects = [subject_id]
    for subject in subjects:
        features = pd.read_csv(
            os.path.join(features_directory, subject, 'features.csv'))
        timings, onsets = OnsetDetector(config=config).apply(features)
        save_path = os.path.join(save_directory, 'output', subject)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        onsets.to_csv(
            os.path.join(save_path, 'onset_detection.csv'), index=False)
        timings.to_csv(
            os.path.join(save_path, 'timings_detection.csv'), index=False)


@cli_algorithm.command(name='plot')
@click.option('--config_file', type=click.Path(exists=False, file_okay=True),
              default=_DEFAULT_CONFIG_PATH,
              help='Set the path to the configuration file')
@click.option('--subject_id', type=click.STRING, default=None,
              help='Set the name of the file to be processed')
@click.option('--processed_data_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_PROCESSED_DATA_PATH,
              help="Set the directory to the processed data")
@click.option('--detection_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_DETECTION_PATH,
              help="Set the directory to the onset detection results")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_RUN_PATH,
              help="Set the directory to save the figures")
@click.option('--font_size', type=click.INT, default=18,
              help='Set the font size of the figure')
@click.option('--db_name', type=click.STRING,
              help='Name of the database', default="test-data")
@click.option('--show', type=click.BOOL,
              help='Set True to display the result', default=False)
def cli_algorithm_plot(
        config_file, subject_id, processed_data_directory,
        detection_directory, save_directory, font_size, db_name, show):
    """Function to visualize the results of the onset detection"""
    config, processed_data_directory = read_config(
        config_file, processed_data_directory)
    if subject_id is None:
        subjects = os.listdir(processed_data_directory)
        subjects = [directory for directory in subjects
                    if '.json' not in directory
                    and directory not in _EXCLUDE_SUBJECTS]
    else:
        subjects = [subject_id]
    for subject in subjects:
        data = pd.read_csv(os.path.join(processed_data_directory,
                                        subject, f'{subject}_data.csv'))
        results = pd.read_csv(
            os.path.join(detection_directory, subject, 'onset_detection.csv'))
        fig = Plotter(
            config=config,
            name=f"{subject} at {db_name} database",
            exclude_signals=_EXCLUDE_SIGNALS,
            font_size=font_size).plot_data(data=data, results=results)
        save_path = os.path.join(save_directory, 'figures', subject)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        fig.write_html(os.path.join(save_path, 'onset_detection.html'))
        if show:
            fig.show()


@cli.command(name='pipeline')
@click.option('--fs', type=click.FLOAT, default=25,
              help="Set the sampling frequency")
@click.option('--run_purpose', type=click.STRING,
              help='Set the purpose of the current run')
@click.option('--features_window_length', type=click.FLOAT, default=5,
              help='Set the length of window used for feature extraction')
@click.option('--config_file', type=click.Path(exists=False, file_okay=True),
              default=_DEFAULT_CONFIG_PATH,
              help='Se the path to save the configuration file')
@click.option('--subject_id', type=click.STRING, default=None,
              help='Set the name of the file to be processed')
@click.option('--data_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_DATA_PATH,
              help="set the directory to the raw data")
@click.option('--processed_data_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_PROCESSED_DATA_PATH,
              help="Set the directory to save the processed data")
@click.option('--detection_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_DETECTION_PATH,
              help="Set the directory to save onset detection results")
@click.option('--features_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_FEATURES_PATH,
              help="Set the directory to save the features")
@click.option('--save_directory',
              type=click.Path(exists=True, file_okay=False),
              default=_DEFAULT_RUN_PATH)
@click.option('--font_size', type=click.INT, default=18,
              help='Set the font size of the figure')
@click.option('--db_name', type=click.STRING,
              help='Name of the database', default="test-data")
@click.option('--show', type=click.BOOL,
              help='Set True to display the result', default=False)
@click.pass_context
def cli_pipeline(
        ctx, fs, run_purpose, features_window_length,
        config_file, subject_id, data_directory, processed_data_directory,
        detection_directory, features_directory, save_directory, font_size,
        db_name, show):
    """Pipeline for the onset detection"""
    ctx.invoke(cli_configure,
               fs=fs,
               save_directory=save_directory,
               run_purpose=run_purpose,
               features_window_length=features_window_length)
    ctx.invoke(cli_data_preprocess,
               config_file=config_file,
               subject_id=subject_id,
               data_directory=data_directory,
               save_directory=processed_data_directory)
    ctx.invoke(cli_data_plot,
               config_file=config_file,
               subject_id=subject_id,
               processed_data_directory=processed_data_directory,
               save_directory=save_directory,
               show=show,
               font_size=font_size,
               db_name=db_name)
    ctx.invoke(cli_features_extract,
               subject_id=subject_id,
               processed_data_directory=processed_data_directory,
               save_directory=save_directory,
               config_file=config_file)
    ctx.invoke(cli_features_plot,
               config_file=config_file,
               subject_id=subject_id,
               features_directory=features_directory,
               processed_data_directory=processed_data_directory,
               save_directory=save_directory,
               font_size=font_size,
               db_name=db_name)
    ctx.invoke(cli_algorithm_apply,
               subject_id=subject_id,
               features_directory=features_directory,
               save_directory=save_directory,
               config_file=config_file)
    ctx.invoke(cli_algorithm_plot,
               config_file=config_file,
               subject_id=subject_id,
               processed_data_directory=processed_data_directory,
               detection_directory=detection_directory,
               save_directory=save_directory,
               font_size=font_size,
               db_name=db_name,
               show=show)
