import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import butter, filtfilt
import os


class TestDataRecord:
    """TestData record.

    Class for records from the TestData dataset.

    """
    def __init__(self, re_sample=False, config=None, exclude_signals=None):
        if exclude_signals is None:
            exclude_signals = []
        self.re_sample = re_sample
        self.config = config
        self.exclude_signals = exclude_signals

    def read(self, directory):
        """Read a record from a csv file.

        Args:
            directory: Directory to a csv file.

        Returns:
            A record.
        """
        files = os.listdir(directory)
        file_names = [file for file in files if ".csv" in file]
        all_data = pd.DataFrame()
        for i, file_name in enumerate(file_names):
            file = os.path.join(directory, file_name)
            data = pd.read_csv(file)
            data.rename(inplace=True, columns={
                'timestamp_relative_[ms]': 'time',
                'acc_x_[m/s^2]': 'acc_x',
                'acc_y_[m/s^2]': 'acc_y',
                'acc_z_[m/s^2]': 'acc_z',
                'gyr_x_[rad/s]': 'gyr_x',
                'gyr_y_[rad/s]': 'gyr_y',
                'gyr_z_[rad/s]': 'gyr_z',
                'mag_x_[uT]': 'mag_x',
                'mag_y_[uT]': 'mag_y',
                'mag_z_[uT]': 'mag_z'})
            data.drop(self.exclude_signals, inplace=True, axis=1)
            data.dropna(how='any', inplace=True)
            data['time'] = data['time'].copy() / 1000
            acc_columns = [column for column in data.columns
                           if 'acc' in column]
            gyr_columns = [column for column in data.columns
                           if 'gyr' in column]
            data[acc_columns] = data[acc_columns].copy() / 9.8
            data[gyr_columns] = data[gyr_columns].copy() * (180 / np.pi)
            data, fs_original = self.recover_missing_values(data)
            if self.re_sample:
                if self.config is None:
                    raise ValueError("config file must be given "
                                     "when down_sample is True")
                else:
                    if self.config['fs'] != fs_original:
                        data = self.re_sampling(
                            data=data, fs_original=fs_original,
                            fs_new=self.config['fs'])
            if i == 0:
                all_data = data.copy()
            else:
                data['time'] = data['time'] + all_data['time'].iloc[-1] + \
                               data['time'].iloc[1] - data['time'][0]
                all_data = pd.concat(
                    [all_data, data], ignore_index=True, sort=False)
        return all_data

    @staticmethod
    def re_sampling(data, fs_original, fs_new=25, start_ind=0, stop_ind=-1):
        """Resample the data"""
        sensor_columns = [column for column in data.columns
                          if 'time' not in column]
        data = data.iloc[start_ind: stop_ind].copy()
        if fs_new < fs_original:
            b, a = butter(2, fs_new / 2, btype='low',
                          output='ba', fs=fs_original)
            for column in sensor_columns:
                data[column] = filtfilt(b, a, data[column])
        timestamp_original = data['time'].values
        timestamp = np.arange(timestamp_original[0], timestamp_original[-1],
                              1 / fs_new)
        resampled_data = pd.DataFrame({'time': timestamp})
        for column in sensor_columns:
            f = interp1d(timestamp_original, data[column],
                         fill_value="extrapolate")
            resampled_data[column] = f(timestamp)
        return resampled_data

    @staticmethod
    def recover_missing_values(data):
        """Recover missing values in the sensory data"""
        sampling_interval = np.nanmedian(data['time'].diff())
        new_timestamps = np.arange(data['time'].iloc[0],
                                   data['time'].iloc[-1],
                                   sampling_interval)
        sensor_columns = [column for column in data.columns
                          if 'time' not in column]
        recovered_data = pd.DataFrame({'time': new_timestamps})
        for column in sensor_columns:
            f = interp1d(data['time'], data[column],
                         fill_value="extrapolate")
            recovered_data[column] = f(new_timestamps)
        return recovered_data, 1 / sampling_interval
