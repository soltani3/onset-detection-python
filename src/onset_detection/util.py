import json
import os
import numpy as np


def read_config(config_file, directory):
    """Function to read the configuration file and correct the directory of
    the processed data according to the sampling frequency"""
    with open(config_file, 'r') as f:
        config = json.load(f)
    directory = os.path.join(directory, f"fs_{config['fs']}")
    with open(os.path.join(directory, 'config_data.json'), 'r') as f:
        config_data = json.load(f)
    if config_data['fs'] != config['fs']:
        raise ValueError("Sampling frequency of data and "
                         "the algorithms do not match")
    return config, directory


def add_norm(data):
    """Add acceleration norm to the data

    Args:
        data: a dataframe containing the acceleration signals
    """
    acc_columns = [column for column in data.columns if 'acc' in column]
    data['acc_norm'] = np.sqrt(
        np.sum(np.power(data[acc_columns], 2), axis=1))
    return data
