import numpy as np
import pandas as pd
from scipy.signal import find_peaks
from scipy.signal import butter
from scipy.signal import filtfilt
import statsmodels.api as sm


class FeatureExtractor:
    """Class to extract features.

    Class to extract IMU-based features for the onset detection

    Attributes:
        config = a .json file containing the configuration of the current run

    """
    _WIN_SHIFT = 1

    def __init__(self, config=None):
        self.config = config

    def apply(self, data):
        """Apply the feature extraction.

        Args:
            data: a dataframe containing sensory data

        """
        acc_columns = [column for column in data.columns if 'acc' in column]
        acc_signal = np.sqrt(np.sum(np.power(data[acc_columns], 2), axis=1))
        acc_signal = acc_signal - 1
        gyr_columns = [column for column in data.columns if 'gyr' in column]
        gyr_signal = np.sqrt(np.sum(np.power(data[gyr_columns], 2), axis=1))
        b, a = butter(N=4, Wn=2, btype='low',
                      fs=self.config['fs'], output='ba')
        acc_signal = filtfilt(b, a, acc_signal)
        gyr_signal = filtfilt(b, a, gyr_signal)
        acc_mean = self._compute_mov_mean(acc_signal)
        time = (np.arange(0, len(acc_mean)) *
                (self._WIN_SHIFT / self.config['fs'])) + data['time'].iloc[0]
        return pd.DataFrame({
            'time': time,
            'acc_mean': acc_mean,
            'acc_std': self._compute_mov_std(acc_signal),
            'acc_df': self._compute_mov_df(acc_signal),
            'acc_df_amp': self._compute_mov_df_amp(acc_signal),
            'acc_auc_peak': self._compute_mov_auc_peak(acc_signal),
            'acc_auc_interval': self._compute_mov_auc_interval(acc_signal),
            'acc_min': self._compute_mov_min(acc_signal),
            'acc_max': self._compute_mov_max(acc_signal),
            'acc_p2p': self._compute_mov_p2p(acc_signal),
            'acc_zc': self._compute_mov_zc(acc_signal),
            'gyr_mean': self._compute_mov_mean(gyr_signal),
            'gyr_std': self._compute_mov_std(gyr_signal),
            'gyr_df': self._compute_mov_df(gyr_signal),
            'gyr_df_amp': self._compute_mov_df_amp(gyr_signal),
        })

    @staticmethod
    def _compute_envelop(signal, config):
        """Compute envelope of an IMU signal"""
        b_hp, a_hp = butter(N=4, Wn=0.1, btype='high',
                            fs=config['fs'], output='ba')
        signal_hp = filtfilt(b_hp, a_hp, signal)
        b_lp, a_lp = butter(N=4, Wn=0.2, btype='low',
                            fs=config['fs'], output='ba')
        envelop = filtfilt(b_lp, a_lp, np.abs(signal_hp))
        return envelop

    def _compute_mov_mean(self, signal):
        """compute a moving mean on an IMU signal"""
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            feature.append(np.nanmean(signal[start: start + win_size]))
        return self._extend_feature(feature, win_size)

    def _compute_mov_std(self, signal):
        """compute a moving std on an IMU signal"""
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            feature.append(np.nanstd(signal[start: start + win_size]))
        return self._extend_feature(feature, win_size)

    def _compute_mov_auc_peak(self, signal):
        """compute peak of a moving autocorrelation on an IMU signal"""
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            lags = np.arange(0, np.min([win_size - 1, 2 * self.config['fs']]))
            auc = sm.tsa.acf(signal[start: start + win_size],
                             nlags=len(lags) - 1)
            peaks_ind, _ = find_peaks(auc)
            if len(peaks_ind) > 0:
                max_peak = np.max(auc[peaks_ind])
            else:
                max_peak = 0
            feature.append(max_peak)
        return self._extend_feature(feature, win_size)

    def _compute_mov_auc_interval(self, signal):
        """compute interval of an IMU signal using a moving autocorrelation"""
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            lags = np.arange(0, np.min([win_size - 1, 2 * self.config['fs']]))
            auc = sm.tsa.acf(signal[start: start + win_size],
                             nlags=len(lags) - 1)
            peaks_ind, _ = find_peaks(auc)
            if len(peaks_ind) > 0:
                max_peak = np.argmax(auc[peaks_ind])
                peak_lags = lags[peaks_ind]
                max_interval = peak_lags[max_peak]
            else:
                max_interval = 0
            feature.append(max_interval)
        return self._extend_feature(feature, win_size)

    def compute_windows(self, signal_length, win_length, fs):
        """compute windows to use in feature extraction"""
        win_size = int(np.round(fs * win_length))
        win_shift = self._WIN_SHIFT
        return np.arange(0, signal_length - win_size + 1, win_shift), win_size

    @staticmethod
    def _extend_feature(feature, win_size):
        """Extend computed features to compensate windowing effect"""
        extended_feature = [0] * (np.floor(win_size / 2).astype(int))
        extended_feature.extend(feature)
        extended_feature.extend([0] * (np.floor(win_size / 2).astype(int)))
        return extended_feature

    def _compute_mov_df(self, signal):
        """compute dominant frequency of an IMU signal using a moving window"""
        def compute_df(samples, fs):
            fft_length = np.max([1000, len(samples)])
            frequencies = np.fft.fftfreq(fft_length) * fs
            frequencies = frequencies[: int(fft_length/2)]
            samples = samples * np.hanning(len(samples))
            spectrum = np.abs(np.fft.fft(samples, fft_length))
            spectrum = spectrum[: int(fft_length / 2)]
            start_ind = np.argmin(np.abs(0.5 - frequencies))
            acceptable_freq = frequencies[start_ind:]
            acceptable_spec = spectrum[start_ind:]
            peaks, _ = find_peaks(acceptable_spec)
            if len(peaks) > 0:
                df = acceptable_freq[peaks[0]]
            else:
                df = np.nan
            """fig = make_subplots(rows=1, cols=1, shared_xaxes=True)
            fig.add_scatter(x=frequencies, y=np.abs(spectrum),
                            row=1, col=1)
            fig.add_vline(x=df, row=1, col=1)
            fig.show()"""
            return df
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            feature.append(compute_df(
                signal[start: start + win_size], self.config['fs']))
        return self._extend_feature(feature, win_size)

    def _compute_mov_df_amp(self, signal):
        """compute the amplitude of dominant frequency of an IMU signal
        using a moving window"""
        def compute_df(samples, fs):
            fft_length = np.max([1000, len(samples)])
            frequencies = np.fft.fftfreq(fft_length) * fs
            frequencies = frequencies[: int(fft_length/2)]
            samples = samples * np.hanning(len(samples))
            spectrum = np.abs(np.fft.fft(samples, fft_length))
            spectrum = spectrum[: int(fft_length / 2)]
            start_ind = np.argmin(np.abs(0.5 - frequencies))
            acceptable_spec = spectrum[start_ind:]
            peaks, _ = find_peaks(acceptable_spec)
            if len(peaks) > 0:
                df_amp = acceptable_spec[peaks[0]]
            else:
                df_amp = np.nan
            return df_amp
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            feature.append(compute_df(
                signal[start: start + win_size], self.config['fs']))
        return self._extend_feature(feature, win_size)

    def _compute_mov_zc(self, signal):
        """compute zero crossing rate of an IMU signal using a moving window"""
        def compute_zc(samples):
            return np.sum(np.abs(np.diff(samples > 0)))
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            feature.append(compute_zc(signal[start: start + win_size]))
        return self._extend_feature(feature, win_size)

    def _compute_mov_min(self, signal):
        """compute a moving minimum of an IMU signal"""
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            feature.append(np.nanmin(signal[start: start + win_size]))
        return self._extend_feature(feature, win_size)

    def _compute_mov_max(self, signal):
        """compute a moving maximum of an IMU signal"""
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            feature.append(np.nanmax(signal[start: start + win_size]))
        return self._extend_feature(feature, win_size)

    def _compute_mov_p2p(self, signal):
        """compute a moving peak-to-peak of an IMU signal"""
        starts, win_size = self.compute_windows(
            len(signal), self.config['algorithm']['features_window_length'],
            self.config['fs'])
        feature = []
        for start in starts:
            feature.append(np.nanmax(signal[start: start + win_size]) -
                           np.nanmin(signal[start: start + win_size]))
        return self._extend_feature(feature, win_size)
