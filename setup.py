from setuptools import find_packages
from setuptools import setup


setup(
    name='onset_detection',
    use_scm_version=True,
    description='Python package to detect onsets of physical activities',
    url='https://gitlab.csem.local/asi1/onset-detection.git',
    author='Ramin Abolfazl SOLTANI',
    author_email='rsoltanislt@gmail.com',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    python_requires='>=3.8',
    install_requires=[
        'click',
        'joblib',
        'numpy',
        'pandas',
        'plotly',
        'scipy',
        'scikit-learn',
        'statsmodels',
    ],
    entry_points={
        'console_scripts': [
            'onset_detection = onset_detection.cli:cli',
        ],
    },
)
